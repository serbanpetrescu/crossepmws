package ws;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sap.conn.jco.JCoDestination;
import com.sap.conn.jco.JCoDestinationManager;
import com.sap.conn.jco.JCoException;
import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoParameterList;
import com.sap.conn.jco.JCoRepository;

public enum RFCPoller {
	INSTANCE;
	private static final Logger LOGGER = LoggerFactory
			.getLogger(RFCPoller.class);

	private RFCPoller() {
	}

	public void start() {
		try {
			// access the RFC Destination "JCoDemoSystem"
			JCoDestination destination = JCoDestinationManager
					.getDestination("SA8_RFC");

			// make an invocation of STFC_CONNECTION in the backend;
			JCoRepository repo = destination.getRepository();
			Caller c = new Caller(destination,
					repo.getFunction("ZWS_CROSS_POLL_TIMEOUT_IGN"));
			c.start();
		} catch (JCoException e) {
			LOGGER.error("JCo Exception: " + e.getMessage());
		}
	}

	private static class Caller extends Thread {
		private JCoFunction f;
		private JCoDestination d;

		public Caller(JCoDestination d, JCoFunction f) {
			this.d = d;
			this.f = f;
		}

		@Override
		public void run() {
			try {
			do {
				JCoParameterList imports = f.getImportParameterList();
				imports.setValue("IV_APP_ID", "ZWS_CROSS_DEMO_IGN_MC");
				imports.setValue("IV_CHANNEL_ID", "/demo");
				f.execute(d);
				JCoParameterList exports = f.getExportParameterList();
				String timeout = exports.getString("EF_TIMEOUT");
				if (timeout.trim().length() == 0) {
					BackendEndpoint.sendRefresh();
				}
			}while(true);
			} catch (JCoException e) {
				LOGGER.error("JCo Exception: " + e.getMessage());
			}
		}
	}
}
